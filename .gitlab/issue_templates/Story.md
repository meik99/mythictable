<!-- Title suggestion: [Story] Name of story -->
<!-- Story issues track work to create new features and provide distinct new value to the user -->

## User Story

As a player/user/DM/developer, I can ... 

## Details

Any related information

## Steps

- [ ] TBD

## Acceptance Criteria (To be checked off **only** during Review)

This should be filled in prior to moving into ~"In Progress", but is not required upon first submitting the ticket.
**This list needs to be reviewed and completed by someone who did not work on the ticket.** Please do not pass Acceptance Criteria on a ticket you contributed code to. Ping #qa in Slack or #mythic-table to let someone know that a ticket is completed and ready for ~Review.

<!-- Some notes on Acceptance Criteria (AC)
If you are having issues creating Acceptance Criteria, please review [Some notes on Acceptance Criteria(AC)](https://gitlab.com/mythicteam/mythictable/-/wikis/qa/acceptace-criteria). Don't hesitate to ask for AC review or assistance in #qa, it's what we're here for.
-->

- [ ] Given: Samples Criteria exists in template
    - [ ] And: it has not been removed
    - [ ] When: the ticket has been submitted
    - [ ] Then: Someone else should update this
    - [ ] And: include acceptable Acceptance Criteria
    
/label ~"type::story"
/weight 2
<!-- A note on weights
0 is trivial. Does not involve code
1 is a button, documentation corrections, a quick fix
2 is a simple well known task
3 is for a somewhat simple task
5 is for a complicated story that requires a number of moving parts
8 is for a significant amount of work but should with perhaps some unknowns or external dependencies
Anything higher than 8 should be broken up into smaller more distinct stories.
-->
